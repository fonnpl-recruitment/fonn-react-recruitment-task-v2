# React recruitment task

In our system, users can select responsible persons for many resources.
We decided to show you part of the work you’ll be responsible for as a frontend developer in the Fonn.

## General description

The main goal is to implement the RFI form (Request for Information) and a member picker.

Each RFI contains the following fields:
- **Question** – The main question that describes the RFI
- **Respondent** – A person who is responsible to answer for the RFI
- **Followers** – People who are interested in the RFI. Basically – observers

## Designs

We prepared designs stored on Abstract platform:

- Prototype: [link](https://share.goabstract.com/a45c9c4f-6905-4974-8c51-6d6b507d55fc?collectionLayerId=4c8a77e8-056c-4a4c-aec5-ed1dddc94523&mode=design&present=true)
- RFI Form: [link](https://share.goabstract.com/5591aee7-b87f-43fb-80d6-421268178640)
- Single-select mode member picker: [link](https://share.goabstract.com/006aa482-d963-4fcc-aaec-139e36592ac2)
- Multi-select mode member picker: [link](https://share.goabstract.com/0e4b9d40-293f-47e0-b397-1777d411db6b)
- Components: [link](https://share.goabstract.com/8f2ccf0e-11e1-44d1-bfff-d2c3e7e6f0f1)

## Task goal

Your goal is to implement:
1. The RFI form (with fields)
2. Generic, reusable member-picker
3. Complete the following requirements / user stories.


### As a User, I want to fill the RFI form to ask the respondent for a business question

Acceptance Criteria
1. The form should allow filling all required fields
2. The *"Create RFI"* button should be disabled by default
3. The *"Create RFI"* button should be enabled when:
   1. The question field is not empty
   2. Respondent is selected
4. Default followers should be populated from API `GET /members/default` endpoint on the init of the form

UX Requirements
1. The form consists of three elements:
   1. Text field for the question (mandatory field)
   2. Respondent picker (mandatory field)
   3. Follower picker (optional field)
2. The primary action button *“Create RFI”* should be disabled by default,
3. The primary action button “Create RFI” should be enabled by filling all mandatory fields.
4. All secondary action buttons should be enabled by default.
5. Followers field, at the start, has always default members selected, which can be changed/removed.
6. The respondent field is empty by default.
7. The selected respondent can be removed or changed.

### As a User, I want to select a single member as an RFI Respondent to be assigned to the RFI

Acceptance Criteria
1. There should be a possibility to fire member-picker in single-select mode
2. The “Create RFI” button should be disabled if no member is picked
3. If there is already picked member, there should be a possibility to change him to another one
4. There should be a possibility to remove the selected member

UX Requirements
1. Modal allows the user to select a single respondent.
2. If there is no selected respondent, the primary action button “set respondent” should be disabled.
   1. The button should be enabled after the user was selected from the list.
3. When a respondent has been chosen, the user should be able to change it by using the “change” button on the RFI form.
   1. The primary action button name in the modal should be changed to: “change respondent”.
   2. A user should not be able to remove the Respondent in the modal view.
   
### As a User, I want to select multiple members as the RFI Followers to notify interested people about the question

Acceptance Criteria
1. There should be a possibility to fire member-picker in multi-select mode
2. Available to select members should be populated from API `GET /members` endpoint
3. There should be a possibility to remove each follower
4. Already selected followers should be showed in member-picker

UX Requirements
1. Modal allows the user to select multiple followers.
2. Chosen followers can be removed and changed directly in the followers' picker modal. When changes on the list were made, the primary action button in the modal changes its name onto “save changes”

### As a PO, I want to have implemented the generic, reusable member picker to reuse it in the whole system

Acceptance Criteria
1. Member picker should be configurable from the code to use multiple or single selection mode.
2. Available to select members should be:
   1. populated from API `GET /members` endpoint
   2. paginated using the endless pagination
3. All selected members should be listed at the top of the list.
4. Avatars should be populated using [Pravatar](https://pravatar.cc/) service.
   1. The avatar URL should be generated on the frontend side (`https://i.pravatar.cc/150?u={USER_ID}`)

### As a User, I want to search members in a member picker to easily find a member (optional)

Acceptance Criteria
1. Member picker despite its current mode should have a possibility to search list of members
2. Search members should call the API endpoint with the additional search parameter.
   1. Searching members should not hide a selected member list

### As a User,  I want to filter members in a member picker by role to easily find a member (optional)

Acceptance Criteria
1. Member picker despite its current mode should have a possibility to filter the list of members by role
2. Filter members by role should call the API endpoint with the additional role parameter.
   1. Filtering members should not hide a selected member list
   
## Technical aspects

This task checks your knowledge in the following areas:

1. Organizing and designing reusable React Components
2. Implementing views using HTML and CSS (or derivatives)
3. Understanding general technical problems related to processing lists and endless pagination;
4. Accessing data from the API

## Code structure:

It is up to you how do you organize code structure. Be prepared for an explanation of why did you choose that one.


## API Specification

You are welcome to use our mock API. We have prepared a **Swagger** specification of the following endpoints:

1. `GET /members` - Get available members list
2. `GET /members/default` - Get default members list

### Running the Mock API

To run the mock api, do the following steps:

```sh
# Use the correct node version
$ nvm use

# Install dependencies
$ npm install

# Run mock API
$ npm run mock-api
```

Finally, you should see the following links:

1. API root (by default: `http://localhost:8000`)
2. Swagger docs (by default: `http://localhost:8000/docs`)

