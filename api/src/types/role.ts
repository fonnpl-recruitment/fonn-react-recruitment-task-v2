export const Role = {
  Contributor: 'contributor',
  ProjectManager: 'project-manager',
  Subcontractor: 'subcontractor',
  Supervisor: 'supervisor',
} as const;

export type RoleType = typeof Role[keyof typeof Role];
