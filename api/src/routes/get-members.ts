import {BadRequest} from "../common/errors/bad-request";
import {PaginationRequestFactory} from "../common/pagination-request-factory";
import {Envelope} from "../common/envelope";
import {Member} from "../types/member";
import {PaginationMeta} from "../types/pagination-meta";
import {member} from "../mock-data/member";
import {Role} from "../types/role";

const allMembers = member.sort((a, b) => a.email < b.email ? -1 : 1);

export const getMembers = (req, res, next) => {
  if (req.query.search && typeof req.query.search !== 'string') {
    throw new BadRequest('wrong_data', 'search should be of type string')
  }

  if (req.query.role && (typeof req.query.role !== 'string' || !Object.values(Role).includes(req.query.role))) {
    throw new BadRequest('wrong_data', `role should be one of: ${Object.values(Role).join(', ')}`)
  }

  const search = req.query.search?.toLowerCase() || null;
  const role = req.query.role?.toLowerCase() || null;
  let results = allMembers;

  if (search) {
    results = results.filter(member =>
      member.firstName?.toLowerCase().indexOf(search) > -1
      || member.lastName?.toLowerCase().indexOf(search) > -1
      || member.email?.toLowerCase().indexOf(search) > -1)
  }

  if (role) {
    results = results.filter(member => member.role === role);
  }

  const pagination = PaginationRequestFactory.buildFromRequest(req, results.length);
  const paginated = results.slice(pagination.getOffset(), pagination.getOffset() + pagination.getLimit());
  const response = new Envelope<Member[], PaginationMeta>(paginated, {pagination});

  res.status(200).json(response);
  next();
}
