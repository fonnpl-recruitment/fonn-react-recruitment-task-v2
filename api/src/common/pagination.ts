export class Pagination {
  public page: Readonly<number>;
  public onPage: Readonly<number>;
  public maxPages: Readonly<number>;
  public previousPage: Readonly<number | null>;
  public nextPage: Readonly<number | null>;
  public totalCount: Readonly<number>;

  constructor(page: number, onPage: number, totalCount: number) {
    if (totalCount < 0) {
      throw new Error('Total count has to be greater or equal 0')
    } else if (onPage < 1 || onPage > 50) {
      throw new Error('On page number has to be between 1 and 50')
    }

    this.onPage = onPage;
    this.totalCount = totalCount;
    this.maxPages = Math.ceil(this.totalCount / this.onPage);
    this.page = page > this.maxPages ? this.maxPages : page < 1 ? 1 : page;
    this.previousPage = page > 1 ? page - 1 : null;
    this.nextPage = page < this.maxPages ? page + 1 : null;

    Object.freeze(this);
  }

  getOffset(): number {
    return (this.page - 1) * this.onPage;
  }

  getLimit(): number {
    return this.onPage;
  }

  toJSON() {
    return {
      totalCount: this.totalCount,
      maxPages: this.maxPages,
      onPage: this.onPage,
      page: this.page,
      nextPage: this.nextPage,
      previousPage: this.previousPage,
    }
  }
}
