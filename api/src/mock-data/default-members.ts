import {Member} from "../types/member";
import {member} from "./member";

const randomizeMembers = (): Member[] => member
  .sort(() => 0.5 - Math.random())
  .slice(0, Math.floor(Math.random() * 10) + 2);

export const defaultMembers: Member[] = randomizeMembers();
