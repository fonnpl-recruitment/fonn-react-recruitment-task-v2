import React, { FC } from 'react';
import { Button } from 'reactstrap';

interface IApp {}

const App: FC<IApp> = ({}) => {
  return (
    <div>
      <h1>Hello</h1>
      <Button color="primary">primary</Button>
    </div>
  );
};

export default App;
